# Gamejam Game Design documents

A place to put the design documents for gamejams that require them. You can find the docs in the corresponding game jam folders. 
This repo is for design documents ONLY, not for code or assets. 

